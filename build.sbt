import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

val flourSettings = Seq(
  organization := "tf.bug",
  name         := "flour",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.9"),
)

lazy val flour =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(flourSettings)
    .jsSettings( /* ... */ )
    .jvmSettings( /* ... */ )

lazy val flourJS = flour.js
lazy val flourJVM = flour.jvm

val batterSettings = Seq(
  organization := "tf.bug",
  name         := "batter",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.9"),
  libraryDependencies ++= Seq(
    "co.fs2" %%% "fs2-core" % "1.0.4",
  ),
)

lazy val batter =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(batterSettings)
    .jsSettings( /* ... */ )
    .jvmSettings( /* ... */ )
    .dependsOn(flour, chocolate)

lazy val batterJS = batter.js
lazy val batterJVM = batter.jvm

val milkSettings = Seq(
  organization := "tf.bug",
  name         := "milk",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
)

lazy val milk =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(milkSettings)
    .jsSettings( /* ... */ )
    .jvmSettings( /* ... */ )

lazy val milkJS = milk.js
lazy val milkJVM = milk.jvm

val chocolateSettings = Seq(
  organization := "tf.bug",
  name         := "chocolate",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
)

lazy val chocolate =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(chocolateSettings)
    .jsSettings( /* ... */ )
    .jvmSettings( /* ... */ )
    .dependsOn(milk)

lazy val chocolateJS = chocolate.js
lazy val chocolateJVM = chocolate.jvm

val cakeSettings = Seq(
  organization := "tf.bug",
  name         := "cake",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
)

lazy val cake =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(cakeSettings)
    .jsSettings( /* ... */ )
    .jvmSettings( /* ... */ )
    .dependsOn(batter)

lazy val cakeJS = cake.js
lazy val cakeJVM = cake.jvm
