package cake.batter

import cake.flour.Selector
import fs2._

trait StreamSelector[F[_], D, A] extends Selector[Stream[F, ?], D, A]
