package cake.flour

import cake.flour.typeclass.{Provider, Select}

trait Selector[F[_], D, A] {

  def data: F[D]

  def get: F[A]

  def apply[T](child: Symbol)(implicit select: Select[F, D], provider: Provider[F, D, T]): Selector[F, D, T] =
    provider.from(select.data(this)(child))

}
