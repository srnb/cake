package cake.flour

import cake.flour.typeclass.Data

trait Implicits {

  implicit def identityTransformer[F[_], D]: Transformer[F, D, D] = new Transformer[F, D, D] {
    override def in(data: F[D])(implicit d: Data[F, D], e: Data[F, D]): F[D] = data
  }

}
