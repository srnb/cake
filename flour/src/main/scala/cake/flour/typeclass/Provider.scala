package cake.flour.typeclass

import cake.flour.Selector

trait Provider[F[_], D, A] {

  def from(data: F[D]): Selector[F, D, A]

}
