package cake.flour.typeclass

import cake.flour.Selector

trait Data[F[_], D] {

  def provideSelector[A](d: F[D]): Selector[F, D, A]

}
