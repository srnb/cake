package cake.flour.typeclass

import cake.flour.Selector

trait Select[F[_], D] {

  def data[A](on: Selector[F, D, A])(to: Symbol): F[D]

}
